pluginManagement {
    repositories {
        gradlePluginPortal()
        mavenCentral()
        jcenter()
        maven(url = "https://maven.fabricmc.net/")
    }
}

rootProject.name = "auto-config"
